#include<stdio.h>
int main()
{
    int a[10][10],trans[10][10],i,j,m,n;
    printf("Enter number of rows and columns:\n");
    scanf("%d%d",&m,&n);
    printf("\nEnter matrix elements:\n");
    for (i=0;i<m;i++)
    {
        for (j=0;j<n;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("\nEntered matrix is:\n");
    for (i=0;i<m;i++)
    {
        for (j=0;j<n;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    for (i=0;i<m;i++)
    {
        for (j=0;j<n;j++)
        {
            trans[j][i]=a[i][j];
        }
    }
    printf("Transpose of the matrix:\n");
    for (i=0;i<m;i++)
    {
        for (j=0;j<n;j++)
        {
            printf("%d\t",trans[i][j]);
        }
        printf("\n");
    }
}