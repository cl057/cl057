#include<stdio.h>
int main()
{
    int n,a[100],i,key,found=0;
    printf("Enter the number of elements in array:");
    scanf("%d",&n);
    printf("Enter %d elements:\n",n);
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the element to be searched:");
    scanf("%d",&key);
    for (i=0;i<n;i++)
    {
        if (a[i]==key)
        {
            found=1;
            break;
        }
    }
    if (found==1)
    {
       printf("Successful search.Element found at position %d",i+1);
    }
    else
    {
      printf("Unsuccessful search.Element not present in the array\n");
    }
       return 0;
    
}